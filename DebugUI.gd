extends Control

var bombs = 0 setget set_bombs
var max_bombs = 99

onready var label = $Label

func set_bombs(value):
	bombs = clamp(value,0,max_bombs)
	label.text = "x " + str(bombs) 
	
func _ready():
	label.text = "x " + str(bombs) 
	self.max_bombs= PlayerStats.maxBombs
	self.bombs = PlayerStats.bombs
	PlayerStats.connect("bombs_changed", self, "set_bombs")
	
