extends CanvasModulate
var isCurrentlyLightning = false
var maxLightning = 400
var threshold = maxLightning - 1
var nightColor = Color(0.55,0.45,0.45)
var lightningColor = Color(0.75,0.75,0.75)
func _ready():
	color=nightColor
	randomize()
func _process(delta):
	if !isCurrentlyLightning and rand_range(0,maxLightning) > threshold:
		lightning()

func lightning():
	$LightningTimer.start()
	color=lightningColor


func _on_LightningTimer_timeout():
	color=nightColor
	isCurrentlyLightning = false
