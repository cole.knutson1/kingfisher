extends Node2D

const EnemyDeathEffect = preload("res://Effects/GrassEffect.tscn")
const Heart = preload("res://Items/Heart.tscn")
const Sphere = preload("res://Items/Sphere.tscn")
const Bomb = preload("res://Items/Bomb.tscn")

# Editor will enumerate with string names.
export(String, "null", "Heart", "Sphere", "Bomb") var item_contained = "null"
func _on_Hurtbox_area_entered(area):
	var enemyDeathEffect = EnemyDeathEffect.instance()
	add_child(enemyDeathEffect)
	enemyDeathEffect.global_position=global_position - Vector2(0,8)
	$Grass.visible = false
	if item_contained == "Heart":
		var heart = Heart.instance()
		get_parent().add_child(heart)
		heart.global_position=global_position
	elif item_contained == "Sphere":
		var sphere = Sphere.instance()
		get_parent().add_child(sphere)
		sphere.global_position=global_position	
	elif item_contained == "Bomb":
		var bomb = Bomb.instance()
		get_parent().add_child(bomb)
		bomb.global_position=global_position	
	


func _on_GrassEffect_animation_finished():
	queue_free()
