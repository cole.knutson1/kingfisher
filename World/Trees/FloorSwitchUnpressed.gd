extends Area2D

export (bool)var isToggle = false
onready var unpressed_sprite = $UnpressedSprite as Sprite
onready var pressed_sprite = $PressedSprite as Sprite
var switch_activated = false
func _on_FloorSwitchUnpressed_body_entered(body):
	pressed_sprite.visible = true
	unpressed_sprite.visible = false
	switch_activated = true

func _on_FloorSwitchUnpressed_body_exited(body):
	if isToggle:
		pressed_sprite.visible = false
		unpressed_sprite.visible = true
		switch_activated = false
