extends Control

var hearts = 4 setget set_hearts
var max_hearts = 4 setget set_max_hearts

onready var label = $Label
onready var heartUiFull = $HeartUIFull

func set_hearts(value):
	hearts = clamp(value,0,max_hearts)
	if heartUiFull != null:
			heartUiFull.rect_size.x = 15 * hearts

func set_max_hearts(value):
	max_hearts = max(value,1)
	
func _ready():
	self.max_hearts = PlayerStats.maxHealth
	self.hearts = PlayerStats.health
	PlayerStats.connect("health_changed", self, "set_hearts")
