extends KinematicBody2D
export var ACCELERATION = 100
const MAX_SPEED = 100
const FRICTION = 300
const ROLL_SPEED = 150
enum {
	MOVE,
	ROLL,
	ATTACK
}

var state = MOVE
var velocity = Vector2(0,0)
var roll_vector = Vector2.DOWN
var stats = PlayerStats
var isInvisibleDueToFlicker = false
var iframeEnabled = false
onready var animationPlayer = $AnimationPlayer
onready var animationTree = $AnimationTree
onready var animationState = animationTree.get("parameters/playback")
onready var swordHitbox = $HitboxPivot/SwordHitbox
onready var hurtbox = $Hurtbox
onready var bow = $Bow
onready var invincibilityFlickerTimer = $InvincibilityFlickerTimer
onready var interactRaycast = $InteractRaycast as RayCast2D
const bomb = preload("res://ActiveBomb.tscn")
signal laid_bomb_down
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	stats.connect("no_health", self, "queue_free")
	connect("laid_bomb_down", stats, "set_bombs")
	animationTree.active = true
	swordHitbox.knockback_vector = roll_vector
	if PlayerLocation.customCoords != Vector2.ZERO:
		global_position = PlayerLocation.customCoords

func _physics_process(delta):
	match state:
		MOVE:
			move_state()
		ROLL:
			roll_state(delta)
		ATTACK:
			attack_state(delta)
		
func move_state():
	var input_vector = Vector2.ZERO
	input_vector.x = Input.get_action_strength("right") -Input.get_action_strength("left")
	input_vector.y = Input.get_action_strength("down") -Input.get_action_strength("up")
	input_vector = input_vector.normalized()
	if input_vector != Vector2.ZERO:
		roll_vector = input_vector
		swordHitbox.knockback_vector = input_vector	
		animationTree.set("parameters/Run/blend_position", input_vector)
		animationTree.set("parameters/Idle/blend_position", input_vector)
		animationTree.set("parameters/Attack/blend_position", input_vector)
		animationTree.set("parameters/Roll/blend_position", input_vector)		
		animationState.travel("Run")
		velocity = input_vector  * ACCELERATION
	else:
		velocity = velocity.move_toward(Vector2.ZERO, FRICTION)
		animationState.travel("Idle")

	move()
	
	if Input.is_action_just_pressed("roll"):
		state=ROLL
	
	if Input.is_action_just_pressed("attack"):
		state = ATTACK
	
	if Input.is_action_just_pressed("use_item"):
		handle_action_pressed()
	
	if Input.is_action_just_pressed("shoot_arrow"):
		normalize_roll_vector()		
		bow.shoot(roll_vector)
func roll_state(delta):
	velocity = roll_vector * ROLL_SPEED
	animationState.travel("Roll")
	move()
	
func attack_state(delta):
	velocity = Vector2.ZERO
	animationState.travel("Attack")
	
func attack_animation_finished():
	state = MOVE

func roll_animation_finished():
	state = MOVE
	
func move():
	velocity = move_and_slide(velocity)


func _on_Hurtbox_area_entered(area):
	if !hurtbox.invincible and !iframeEnabled:
		stats.health-=1
		hurtbox.start_invincibility(1.5)
		hurtbox.create_hit_effect()
		invincibilityFlickerTimer.start()
		on_take_damage()

func on_take_damage():
	while hurtbox.invincible:
		$Sprite.self_modulate.a = 0
		yield(get_tree().create_timer(.1), "timeout")
		$Sprite.self_modulate.a = 1.0
		yield(get_tree().create_timer(.1), "timeout")

func set_iframe(value):
	iframeEnabled = value

func handle_action_pressed():
	if PlayerStats.bombs > 0:
		var new_bomb = bomb.instance()
		get_parent().add_child(new_bomb)
		new_bomb.global_position = global_position
		emit_signal("laid_bomb_down",-1)

func normalize_roll_vector():
	if roll_vector != Vector2.LEFT and roll_vector != Vector2.RIGHT and roll_vector != Vector2.DOWN and roll_vector != Vector2.UP:
		if roll_vector.x > 0:
			roll_vector = Vector2.RIGHT
		else:
			roll_vector = Vector2.LEFT
