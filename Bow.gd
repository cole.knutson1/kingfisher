extends Node2D

func shoot(input_vector):
	var shot = load("res://Arrow.tscn").instance() as Node2D
	get_tree().root.add_child(shot)
	shot.position = global_position
	shot.velocity = -(shot.position-shot.position-input_vector).normalized()
	if input_vector == Vector2.LEFT:
		shot.rotation_degrees=-90
	if input_vector == Vector2.UP:
		shot.rotation_degrees=0
	if input_vector == Vector2.DOWN:
		shot.rotation_degrees=180
	if input_vector == Vector2.RIGHT:
		shot.rotation_degrees=90
