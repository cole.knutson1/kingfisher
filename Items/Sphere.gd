extends Area2D
signal spherePickedUp(value)
func _ready():
	connect("spherePickedUp", PlayerStats, "set_spheres")
	$AnimationPlayer.play("Animation")

func _on_Sphere_body_entered(body):
	emit_signal("spherePickedUp", 1)
	queue_free()
