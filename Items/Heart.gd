extends Area2D
signal heartPickedUp(value)
func _ready():
	connect("heartPickedUp", PlayerStats, "set_health")
	$AnimationPlayer.play("Animation")

func _on_Heart_body_entered(body):
	emit_signal("heartPickedUp", PlayerStats.health+1)
	queue_free()
