extends Sprite
signal player_picked_up_bomb

func _ready():
	connect("player_picked_up_bomb", PlayerStats, "set_bombs")

func _on_Area2D_body_entered(body):
	emit_signal("player_picked_up_bomb",1)
	queue_free()


func _on_ExplosionSprite_animation_finished():
	pass # Replace with function body.
