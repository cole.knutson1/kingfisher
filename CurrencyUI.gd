extends Control

var spheres = 0 setget set_spheres
var max_spheres = 99

onready var label = $Label

func set_spheres(value):
	spheres = clamp(value,0,max_spheres)
	label.text = "x " + str(spheres) 
	
func _ready():
	$AnimationPlayer.play("Animation")
	label.text = "x " + str(spheres) 
	self.max_spheres = PlayerStats.maxSpheres
	self.spheres = PlayerStats.spheres
	PlayerStats.connect("spheres_changed", self, "set_spheres")
	
