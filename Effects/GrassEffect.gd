extends AnimatedSprite

signal _on_GrassEffect_animation_finished
func _ready():
	var test = get_parent()
	connect("_on_GrassEffect_animation_finished",get_parent(),"_on_GrassEffect_animation_finished")
	frame=0
	play("Animate")
func _on_GrassEffect_animation_finished():
	emit_signal("_on_GrassEffect_animation_finished")
