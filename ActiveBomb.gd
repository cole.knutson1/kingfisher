extends Node2D

onready var animation_player = $AnimationPlayer

func _ready():
	animation_player.play("Animation")


func _on_AnimationPlayer_animation_finished(anim_name):
	$Explosion.visible = true
	$Sprite.visible = false
	$Explosion.frame = 0
	$Explosion.play()


func _on_Explosion_animation_finished():
	queue_free()
