extends Node2D

export(int) var maxHealth = 2
export(int) var maxSpheres = 2
export(int) var maxBombs = 10

onready var health = maxHealth setget set_health
onready var spheres = 0
var bombs = 0
signal no_health
signal health_changed(value)
signal spheres_changed(value)
signal bombs_changed(value)

func set_health(value):
	health = value
	if health > maxHealth:
		health = maxHealth
	if health <= 0:
		emit_signal("health_changed", value)		
		emit_signal("no_health")
	else:
		emit_signal("health_changed", value)

func set_spheres(value):
	spheres+=value
	emit_signal("spheres_changed",spheres)
	
func set_bombs(value):
	bombs+=value
	emit_signal("bombs_changed",bombs)	
