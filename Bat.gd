extends KinematicBody2D
var knockback = Vector2.ZERO
onready var stats = $Stats
onready var playerDetectionZone = $PlayerDetectionZone
onready var hurtbox = $Hurtbox
export var ACCELERATION = 200
export var MAX_SPEED = 75
export var FRICTION = 100
export var hitEffectOffset = Vector2(0,800)
enum {
	IDLE,
	WANDER,
	CHASE
}
var state = IDLE

var velocity = Vector2.ZERO
const EnemyDeathEffect = preload("res://Effects/EnemyDeathEffect.tscn")


func _on_Hurtbox_area_entered(area):
	stats.health-= area.damage
	knockback = area.knockback_vector * 150
	hurtbox.create_hit_effect()


func _physics_process(delta):
	knockback = knockback.move_toward(Vector2.ZERO, 200*delta)
	knockback = move_and_slide(knockback)
	match state:
		IDLE:
			velocity = velocity.move_toward(Vector2.ZERO, FRICTION*delta)
			seek_player()
		WANDER:
			pass
		
		CHASE:
			var player = playerDetectionZone.player
			if player != null:
				var direction = (player.global_position-global_position).normalized()
				velocity = velocity.move_toward(direction * MAX_SPEED,ACCELERATION * delta)
			else:
				state=IDLE
			$AnimatedSprite.flip_h = velocity.x<0
	velocity = move_and_slide(velocity)

func seek_player():
	if playerDetectionZone.can_see_player():
		state = CHASE
func _on_Stats_no_health():
	queue_free()
	var enemyDeathEffect = EnemyDeathEffect.instance()
	get_parent().add_child(enemyDeathEffect)
	enemyDeathEffect.global_position=global_position - hitEffectOffset
