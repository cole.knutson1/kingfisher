extends Area2D

const speed = 5
var target
var velocity = Vector2()
	
func _process(delta):
	position += velocity * speed
	pass

func _on_Shot_area_entered(area):
	queue_free()
