extends Area2D
signal onSceneEntrance(value, coords)
export var ChangeToScene = ""
export var CustomCoordinates = Vector2.ZERO

func _ready():
	connect("onSceneEntrance",PlayerLocation,"change_scene")

func _on_Door_body_entered(body):
	emit_signal("onSceneEntrance", ChangeToScene, CustomCoordinates)
