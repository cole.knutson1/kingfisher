extends Node2D

onready var HOTEL_SCENE = "res://Buildings/HotelInterior.tscn"
onready var OUTSIDE_SCENE = "res://Outside.tscn"
onready var MIMZY_SCENE = "res://Mimzys Room.tscn"
var customCoords = Vector2.ZERO
func _physics_process(delta):
	if Input.is_action_pressed("quit"):
		get_tree().quit()
	if Input.is_action_just_pressed("toggle_fullscreen"):
		OS.window_fullscreen = !OS.window_fullscreen

		

func change_scene(value, coords):
	#Instantiate/Add new scene to world	
	var newScene
	match(value):
		"HOTEL_SCENE":
			newScene = HOTEL_SCENE
		"OUTSIDE_SCENE":
			newScene = OUTSIDE_SCENE
		"MIMZY_SCENE":
			newScene = MIMZY_SCENE
	customCoords = coords
	get_tree().change_scene(newScene)
